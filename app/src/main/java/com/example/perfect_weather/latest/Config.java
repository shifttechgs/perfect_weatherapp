package com.example.perfect_weather.latest;

public class Config {
    public static final String SHARED_PREF_NAME = "prefs";
    public static final String PLACES_SHARED_PREF = "places";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
}


