package com.example.perfect_weather.model;

import java.util.List;

public class MyList {
    public int dt;
    public com.example.perfect_weather.model.Main main;
    public List<Weather> weather;
    public com.example.perfect_weather.model.Clouds clouds;
    public com.example.perfect_weather.model.Wind wind;
    public com.example.perfect_weather.model.Rain rain;
    public com.example.perfect_weather.model.Sys sys ;
    public String dt_txt;

    public MyList() {
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public com.example.perfect_weather.model.Main getMain() {
        return main;
    }

    public void setMain(com.example.perfect_weather.model.Main main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public com.example.perfect_weather.model.Clouds getClouds() {
        return clouds;
    }

    public void setClouds(com.example.perfect_weather.model.Clouds clouds) {
        this.clouds = clouds;
    }

    public com.example.perfect_weather.model.Wind getWind() {
        return wind;
    }

    public void setWind(com.example.perfect_weather.model.Wind wind) {
        this.wind = wind;
    }

    public com.example.perfect_weather.model.Rain getRain() {
        return rain;
    }

    public void setRain(com.example.perfect_weather.model.Rain rain) {
        this.rain = rain;
    }

    public com.example.perfect_weather.model.Sys getSys() {
        return sys;
    }

    public void setSys(com.example.perfect_weather.model.Sys sys) {
        this.sys = sys;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }
}
