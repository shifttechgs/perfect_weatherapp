package com.example.perfect_weather.model;

import java.util.List;

public class WeatherForecastResult {
    public String cod ;
    public double message;
    public int cnt;
    public List<com.example.perfect_weather.model.MyList> list;
    public com.example.perfect_weather.model.City city;

    public WeatherForecastResult() {
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<com.example.perfect_weather.model.MyList> getList() {
        return list;
    }

    public void setList(List<com.example.perfect_weather.model.MyList> list) {
        this.list = list;
    }

    public com.example.perfect_weather.model.City getCity() {
        return city;
    }

    public void setCity(com.example.perfect_weather.model.City city) {
        this.city = city;
    }
}
